cmake_minimum_required(VERSION 2.8)
project(tree_distribution C)

set(CMAKE_C_STANDARD 11)

find_package(OpenMP REQUIRED)

if (OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
endif ()

add_executable(tree_distribution matrix_mul.c)